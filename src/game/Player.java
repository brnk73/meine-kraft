package game;
import processing.core.PGraphics;

public class Player extends Movable implements Thing {

	private int sxpos, sypos, szpos;
	private int camheight;
	private float gravity;
	private boolean isonground;
	private boolean sneaky;
	private int lookx = -1;
	private int looky = -1;
	private int lookz = -1;
	
	


	public Player(int xpos, int ypos, int zpos) {
		super(xpos, ypos,zpos);
		
		isonground = true;
		gravity = 1.5f;
		
//		setXSize(60);
//		setYSize(160);
//		setZSize(60);
		
		setXSize(400);
		setYSize(1800);
		setZSize(400);
		camheight = 1650;
	}
	
	public void reset() {
		setXPos(sxpos);
		setYPos(sypos);
		setZPos(szpos);
		setXmom(0f);
		setYmom(0f);
		setZmom(0f);
	}
	

	
	public void gravity() {
		if((getZPos()+getZmom()+getZacc())<0.0f) 
			setZacc(getZacc() + gravity);
		else {	
			isonground = true;
			setZacc(0f);	
			setZmom(0f);
			setZPos(0);
		}
	}
	
	public void draw(PGraphics world) {
		
	}
	
	public int getCamheight() {
		return camheight;
	}

	public void setCamheight(int camheight) {
		this.camheight = camheight;
	}

	public float getGravity() {
		return gravity;
	}

	public void setGravity(float gravity) {
		this.gravity = gravity;
	}

	public boolean isSneaky() {
		return sneaky;
	}

	public void setSneaky(boolean sneaky) {
		this.sneaky = sneaky;
	}

	public int getLookx() {
		return lookx;
	}

	public void setLookx(int lookx) {
		this.lookx = lookx;
	}

	public int getLooky() {
		return looky;
	}

	public void setLooky(int looky) {
		this.looky = looky;
	}

	public int getLookz() {
		return lookz;
	}

	public void setLookz(int lookz) {
		this.lookz = lookz;
	}

	public void setIsonground(boolean isonground) {
		this.isonground = isonground;
	}
	
	public boolean isOnGround() {
		return isonground;
	}
}
