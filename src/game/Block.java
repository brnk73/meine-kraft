package game;

public enum Block {
	STONE, DIRT, GRASS, WOOD, WOODPLANKS, LEAVES, TALLGRASS;

}
