package game;

public abstract class BasicStructure implements Structure  {
	private int xpos, ypos, zpos;
	private int[][][] data;
	
	public int getXpos() {
		return xpos;
	}
	public void setXpos(int xpos) {
		this.xpos = xpos;
	}
	public int getYpos() {
		return ypos;
	}
	public void setYpos(int ypos) {
		this.ypos = ypos;
	}
	public int getZpos() {
		return zpos;
	}
	public void setZpos(int zpos) {
		this.zpos = zpos;
	}
	public int[][][] getData() {
		return data;
	}
	public void setData(int[][][] data) {
		this.data = data;
	}
	


	
}
