package game;
import java.util.HashMap;
import java.util.Map;


public class TextureManager {
	
	private static final TextureManager loader = new TextureManager();
	
	private Map<String, ImageSet> textures = new HashMap<>();

	
	private TextureManager() {}
	

	public static TextureManager getLoader() {
		return loader;
	}
	
	public void addImageSet(String name, ImageSet set) {
		this.textures.put(name, set);
	}
	
	public ImageSet getImageSet(String name) {
		return this.textures.get(name);
	}
}
