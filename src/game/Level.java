package game;
import java.util.ArrayList;
import java.util.Iterator;

import processing.core.PGraphics;


public class Level {

	private ArrayList<Thing> walls = new ArrayList<Thing>();
	private ArrayList<Thing> inactive = new ArrayList<Thing>();
	private int[][][] pane = new int[255][127][255];
	public ArrayList<Structure> structures = new ArrayList<Structure>();


		
	public void init() {
		generateTerrain();
		
		for (int y=0; y<structures.size(); y++)										//for every Structure
			for (int s=0; s<structures.get(y).getData().length; s++)				//for Y
				for (int i=0; i<structures.get(y).getData()[0].length; i++)			//for X
					for (int d=0; d<structures.get(y).getData()[0][0].length; d++) 	//for Z
					{
						if ( //World Boundary Check -- this is bad
							structures.get(y).getXpos()+i >0 && structures.get(y).getZpos()+s >0 && structures.get(y).getYpos()+d >0 &&
							structures.get(y).getXpos()+i <pane.length && structures.get(y).getZpos()+s <pane[0].length && structures.get(y).getYpos()+d <pane[0][0].length
						)
							
								pane[structures.get(y).getXpos()+i][structures.get(y).getZpos()+s][structures.get(y).getYpos()+d] = 
								structures.get(y).getData()[s][i][d];
								//copy the structure into the world
					}

		
		
		
		for(int s=0; s<getPane().length; s++)
			for(int i=0; i<getPane()[0].length; i++)
				for(int d=0; d<getPane()[0][0].length; d++) {

					if (getPane()[s][i][d]!=0) {
						getInactive().add(new BasicThing(	//preload the cleaner
							 BasicThing.defaultSize*s, 
							-BasicThing.defaultSize*i,
							 BasicThing.defaultSize*d,
							TextureManager.getLoader().getImageSet(Block.values()[getPane()[s][i][d]-1].name().toLowerCase())
						));
//					System.out.println(Block.values()[pane[s][i][d]-1].name().toLowerCase());
					}
				}	
	}
	
	public void update (int x, int y, int z, int blockID) {
		getPane()[x][y][z] = blockID;
		for (int i=0 ; i<getWalls().size() ; i++) {
			if (getWalls().get(i).getXPos()/getWalls().get(i).defaultSize == x && getWalls().get(i).getYPos()/getWalls().get(i).defaultSize == -y && getWalls().get(i).getZPos()/getWalls().get(i).defaultSize == z ) {
				if (blockID != 0)
					getWalls().get(i).setImages(TextureManager.getLoader().getImageSet(Block.values()[blockID-1].name().toLowerCase()));
				else 
					getWalls().remove(i);
				return;
			}
		}
		switch (blockID-1) {
			case 6: 	getWalls().add(new ItemThing(
							 ItemThing.defaultSize*x, 
							-ItemThing.defaultSize*y, 
							 ItemThing.defaultSize*z, 
							TextureManager.getLoader().getImageSet(Block.values()[blockID-1].name().toLowerCase()).side)); break;
			default:	getWalls().add(new BasicThing(
							 BasicThing.defaultSize*x, 
							-BasicThing.defaultSize*y,
							 BasicThing.defaultSize*z,
							TextureManager.getLoader().getImageSet(Block.values()[blockID-1].name().toLowerCase())
							));	break;
	
		}
	}
	
		
    public void createlvl(PGraphics g) { //only show sides of blocks which are visible
    	int[][][] tempPane = getPane();
    	int tempCords;
    	int sides;
    	 
    	for (Thing obb : (ArrayList<Thing>)getWalls().clone()) {

    		sides = 0;
    		
    		
    		if (	tempPane[0][0].length<obb.getZPos()/obb.defaultSize+2	|| 
    				(tempCords=tempPane[(int) (obb.getXPos()/obb.defaultSize)][(int) (-obb.getYPos()/obb.defaultSize)][(int) (obb.getZPos()/obb.defaultSize+1)])==0 ||
    				tempCords>=6) 
    			
    				sides+=0b00_00_01;
    		
    		if (	obb.getZPos()/obb.defaultSize<1					|| 
    				(tempCords=tempPane[(int) (obb.getXPos()/obb.defaultSize)][(int) (-obb.getYPos()/obb.defaultSize)][(int) (obb.getZPos()/obb.defaultSize-1)])==0	||
    				tempPane[(int) (obb.getXPos()/obb.defaultSize)][(int) (-obb.getYPos()/obb.defaultSize)][(int) (obb.getZPos()/obb.defaultSize-1)]>=6) 
    			
    				sides+=0b00_00_10;
    		
    		if (	tempPane.length<obb.getXPos()/obb.defaultSize+2 		|| 
    				(tempCords=tempPane[(int) (obb.getXPos()/obb.defaultSize+1)][(int) (-obb.getYPos()/obb.defaultSize)][(int) (obb.getZPos()/obb.defaultSize)])==0	||
    				tempCords>=6	) 
    			
    				sides+=0b00_01_00;
    		
    		if (	obb.getXPos()/obb.defaultSize<1 				|| 
    				(tempCords=tempPane[(int) (obb.getXPos()/obb.defaultSize-1)][(int) (-obb.getYPos()/obb.defaultSize)][(int) (obb.getZPos()/obb.defaultSize)])==0 ||
    				tempCords>=6) 
    			
    				sides+=0b00_10_00;
    		
    		if (	tempPane[0].length-2<-obb.getYPos()/obb.defaultSize 	|| 
    				(tempCords=tempPane[(int) (obb.getXPos()/obb.defaultSize)][(int) ((-obb.getYPos()/obb.defaultSize)+1)][(int) (obb.getZPos()/obb.defaultSize)])==0 ||
    				tempCords>=6) 
    				
    				sides+=0b01_00_00;
    		if (	obb.getYPos()/obb.defaultSize<-1 				&& 
    				((tempCords=tempPane[(int) (obb.getXPos()/obb.defaultSize)][(int) ((-obb.getYPos()/obb.defaultSize)-1)][(int) (obb.getZPos()/obb.defaultSize)])==0 ||
    				tempCords>=6)) 
    			
    				sides+=0b10_00_00;
    		
    		obb.draw(g, sides);
    	
    	}
    }
    
   
    public void generateTerrain() {
		getPane()[124][4][133]=4;
		
		float r1 = (float) Math.random();
		float r2 = (float) Math.random();

		float r4 = (float) Math.random();
		float r5 = (float) Math.random();
		for(int s=0; s<255; s++)
			for(int d=0; d<255; d++) {
				
				for (int f=0; f<7; f++)
				{
					getPane()[s][f][d] = 1;
				}
				for (int f=5; f<16; f++)
				{
					getPane()[s][(int) (Math.sin((s/3+r4*2)*r1)*0.2f*f*Math.cos((d/3+r5*2)*r2)*2)+f][d] 
							= 1+(int)(f*f*(3f/300f));
				}
			}
		
		
		boolean[][] densMap = new boolean[255][255];
		int s=0; int d=0;
		for(int i=0; i<800;) {
			s = (int) (Math.random()*255);  d = (int) (Math.random()*255);
				if ( !densMap[s][d] ) {
					densMap[s][d] = true; i++;
				structures.add(new Tree(s, d, (int) (Math.sin((s/3+r4*2)*r1)*3.2f*Math.cos((d/3+r5*2)*r2)*2)+16));
				}
					
			}
    }

	public int[][][] getPane() {
		return pane;
	}

	public void setPane(int[][][] pane) {
		this.pane = pane;
	}

	public ArrayList<Thing> getWalls() {
		return walls;
	}

	public void setWalls(ArrayList<Thing> walls) {
		this.walls = walls;
	}

	public ArrayList<Thing> getInactive() {
		return inactive;
	}

	public void setInactive(ArrayList<Thing> inactive) {
		this.inactive = inactive;
	}

	
}
