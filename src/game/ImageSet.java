package game;
import processing.core.PImage;

public class ImageSet {
	public PImage top;
	public PImage side;
	public PImage bottom;
	

	public ImageSet(PImage top, PImage side, PImage bottom) {
		this.top = top;
		this.side = side;
		this.bottom = bottom;

	}
}
