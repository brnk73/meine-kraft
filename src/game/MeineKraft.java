package game;

import java.util.ArrayList;
import com.jogamp.newt.opengl.GLWindow;
import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import processing.opengl.PGraphicsOpenGL;

public class MeineKraft extends PApplet
{

	private ArrayList<KeyEvent> keycodes = new ArrayList<KeyEvent>();

	static int WIDTH = 1200, HEIGHT = 680;

	private Player p = new Player(320, 320, -32768);
	private Level lvl = new Level();
	private Robot rob;
	private float x = 0f;
	private float y = 0f;
	private float armDistance = 0f;
	private float vboAngle = 0.21f * PI;
	private int currBBlock = 3;
	private int renderDistance = 40000;
	private long lastCleanTime;
	private long lastTickTime;
	private int tickTime = 20;
	private int cleanTime = 200;
	private boolean debug = true;
	
	private PGraphics worldView, guiView;

	Thread ticker=new Thread(new Runnable()
	{
		@Override 
		public void run()
		{
			while(true)
			{
				tick();
				try
				{
					Thread.sleep(
								tickTime < (System.currentTimeMillis()-lastTickTime) ? 0 : tickTime-(System.currentTimeMillis()-lastTickTime)
								);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
				lastTickTime=System.currentTimeMillis();	
			}
		}
	});
	
	Thread cleaner=new Thread(new Runnable()
	{
		@Override 
		public void run()
		{
			while(true)
			{
				clean();
				try
				{
					Thread.sleep(
							cleanTime < (System.currentTimeMillis()-lastCleanTime) ? 0 : cleanTime-(System.currentTimeMillis()-lastCleanTime)
								);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
				lastCleanTime=System.currentTimeMillis();	
			}
		}
	});

	public static void main(String[] args)
	{
		String[] appArgs = { "Meine Kraft Game" };
		MeineKraft game = new MeineKraft();
		PApplet.runSketch(appArgs, game);

	}

	@Override
	public void mouseWheel(MouseEvent event)
	{
		super.mouseWheel(event);
		currBBlock = (currBBlock + 1) % Block.values().length;
		System.out.println("Current Block : " + Block.values()[currBBlock]);
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		super.keyPressed();

		for (KeyEvent event : keycodes)
			if (e.getKeyCode() == event.getKeyCode())
				return;
		keycodes.add(e);

	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		super.keyTyped();
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		super.keyReleased();
		for (int i = 0; i < keycodes.size(); i++)
		{
			if (e.getKeyCode() == keycodes.get(i).getKeyCode())
				keycodes.remove(i);
		}
	}
	

	@Override
	public void mousePressed(MouseEvent event)
	{
		super.mousePressed(event);

		if (p.getLookx() > -1)
		{
			if (event.getButton() == 37)
				lvl.update(p.getLookx(), p.getLooky(), p.getLookz(), 0);
			else
				setBlock(currBBlock + 1);
		}
	}

	@Override
	public void mouseMoved(MouseEvent event)
	{
		// TODO Auto-generated method stub
		super.mouseMoved(event);
		if (focused)
		{

			Point currentMousePos = MouseInfo.getPointerInfo().getLocation();	
			rob.mouseMove((WIDTH/2 + getCanvas().getX()), (HEIGHT/2 + getCanvas().getY()) );
			
			x += (currentMousePos.x - (WIDTH/2 + getCanvas().getX())) / 360f;
			y += (currentMousePos.y - (HEIGHT/2 + getCanvas().getY())) / 360f;
		}
	}

	// Get the GL window for this sketch
	public GLWindow getCanvas() {
	  return (GLWindow) surface.getNative();
	}
	
	public void settings()
	{
		size(WIDTH, HEIGHT, P2D);
	

	}

	public void setup()
	{
		worldView = createGraphics(WIDTH, HEIGHT, P3D);
		guiView = createGraphics(WIDTH, HEIGHT, P2D);
		
//		hint(DISABLE_TEXTURE_MIPMAPS);
		( (PGraphicsOpenGL) worldView ).textureSampling(3);
		( (PGraphicsOpenGL) guiView ).textureSampling(3);
		
		
		iniMouse();
		worldView.imageMode(CENTER);
		worldView.rectMode(CENTER);
//		worldView.frustum(-10, 0, 0, 10, 10, 200);

		loadSets();
		lvl.init();
		ticker.start();
//		cleaner.start();

	}

	public void loadSets()
	{
		for (Block s : Block.values())
		{
			String block = s.name().toLowerCase();
			TextureManager.getLoader().addImageSet(block,
					new ImageSet(loadImage("textures/" + block + "_top.png", "png"),
							loadImage("textures/" + block + ".png", "png"),
							loadImage("textures/" + block + "_bottom.png", "png")));
		}
	}

	public void iniMouse()
	{
		try
		{
			rob = new Robot();
		} catch (AWTException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Rectangle screenBounds = MouseInfo.getPointerInfo()
											.getDevice()
											.getDefaultConfiguration()
											.getBounds();
		
		//Move Window so getCanvas() has right values
		windowMove(	screenBounds.x + screenBounds.width / 2 - WIDTH/2, 
					screenBounds.y + screenBounds.height / 2 - (HEIGHT+75)/2);
		noCursor();
	}

//	oof...
	public void collision()
	{

		int[][][] tempPane = lvl.getPane();

		if (p.getXPos() + p.getXmom() < 0 || p.getZPos() + p.getZmom() > 0 || p.getYPos() + p.getYmom() < 0
				||  (p.getXPos() + p.getXmom() + p.getXacc()) / Thing.defaultSize > lvl.getPane().length
				|| -(p.getZPos() + p.getZmom() + p.getZacc()) / Thing.defaultSize > lvl.getPane()[0].length
				||  (p.getYPos() + p.getYmom() + p.getYacc()) / Thing.defaultSize > lvl.getPane()[0][0].length)
		{
			System.out.println("Out of bounds!");
			return;
		}

		int tempCPos = tempPane	[(int) (p.getXPos() + p.getXmom() + p.getXacc() + p.getXSize() + 0f) / Thing.defaultSize]
								[-p.getZPos() / Thing.defaultSize]
								[p.getYPos() / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
			p.setXmom(0f);
			p.setXacc(0f); // Collision on X-Axis
		}
		tempCPos = tempPane	[(int) (p.getXPos() + p.getXmom() + p.getXacc() - p.getXSize() + 0f) / Thing.defaultSize]
							[-p.getZPos() / Thing.defaultSize]
							[p.getYPos() / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
			p.setXmom(0f);
			p.setXacc(0f); // Collision on X-Axis
		}
		tempCPos = tempPane	[(int) (p.getXPos() + p.getXmom() + p.getXacc() + p.getXSize() + 0f) / Thing.defaultSize]
							[(-p.getZPos() + p.getYSize()) / Thing.defaultSize]
							[p.getYPos() / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
		p.setXmom(0f);
		p.setXacc(0f); // Collision on X-Axis
		}
		tempCPos = tempPane	[(int) (p.getXPos() + p.getXmom() + p.getXacc() - p.getXSize() + 0f) / Thing.defaultSize]
							[(-p.getZPos() + p.getYSize()) / Thing.defaultSize]
							[p.getYPos() / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
		p.setXmom(0f);
		p.setXacc(0f); // Collision on X-Axis
		}
				
		
		tempCPos = tempPane	[p.getXPos() / Thing.defaultSize]
							[-p.getZPos() / Thing.defaultSize]
							[(int) (p.getYPos() + p.getYmom() + p.getYacc() + p.getZSize() + 0f) / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
		p.setYmom(0f);
		p.setYacc(0f); // Collision on Z-Axis
		}
		tempCPos = tempPane	[p.getXPos() / Thing.defaultSize]
							[-p.getZPos() / Thing.defaultSize]
							[(int) (p.getYPos() + p.getYmom() + p.getYacc() - p.getZSize() + 0f) / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
		p.setYmom(0f);
		p.setYacc(0f); // Collision on Z-Axis
		}
		tempCPos = tempPane	[p.getXPos() / Thing.defaultSize]
							[(-p.getZPos() + p.getYSize()) / Thing.defaultSize]
							[(int) (p.getYPos() + p.getYmom() + p.getYacc() + p.getZSize() + 0f) / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
		p.setYmom(0f);
		p.setYacc(0f); // Collision on Z-Axis
		}
		tempCPos = tempPane	[p.getXPos() / Thing.defaultSize]
							[(-p.getZPos() + p.getYSize()) / Thing.defaultSize]
							[(int) (p.getYPos() + p.getYmom() + p.getYacc() - p.getZSize() + 0f) / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
		p.setYmom(0f);
		p.setYacc(0f); // Collision on Z-Axis
		}
		
		tempCPos = tempPane	[p.getXPos() / Thing.defaultSize]
							[(int) -(p.getZPos() + p.getZmom() + p.getZacc() + 0f) / Thing.defaultSize]
							[p.getYPos() / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
			p.setZmom(0f);
			p.setZacc(0f);
			p.setIsonground(true); // Collision on Y-Axis
		}
		tempCPos = tempPane	[p.getXPos() / Thing.defaultSize]
							[(int) -(p.getZPos() + p.getZmom() + p.getZacc() - p.getYSize() + 0f) / Thing.defaultSize]
							[p.getYPos() / Thing.defaultSize];
		if (tempCPos != 0 && tempCPos <= 6)
		{
			p.setZmom(0f);
			p.setZacc(0f); // Collision on Y-Axis
		}

	}
	
	public void draw()
	{
		background(0);
		drawWorld(worldView);
		drawGUI(guiView);

		image(worldView, 0, 0);
		
		image(guiView, 0, 0);
	}

	public void drawGUI(PGraphics guiView)
	{
		guiView.beginDraw();
		guiView.clear();

		guiView.textSize(20);
		
		guiView.text("FPS: "+(int)(frameRate), 10, 20);
		
		if (debug)
		{
			guiView.text("X: "+p.getXPos()+" Y: "+p.getYPos()+" Z: "+p.getZPos(), 10, 40);
			guiView.text("Loaded: "+lvl.getWalls().size()+ " Unloaded: "+lvl.getInactive().size(), 10, 60);
			guiView.text("Looking at X: "+p.getLookx()+" Y: "+p.getLooky()+" Z: "+p.getLookz(), 10, 80);
		}
		
		guiView.image(TextureManager.getLoader().getImageSet(Block.values()[currBBlock].name().toLowerCase()).side, 
				WIDTH-72, 8 , 64, 64);
		guiView.endDraw();
	}
	
	public void drawWorld(PGraphics worldView)
	{
//    	   	lights();
//    		System.out.println(keycodes.size());
		worldView.beginDraw();
//		worldView.clear();	
		
//		Set perspective
		worldView.perspective(PI / 3f, ((float)width)/((float)height), 1f, 100000f);

//		fixes grass and leaves transparency at the cost of performance, not advised
//		worldView.hint(ENABLE_DEPTH_SORT);

		worldView.background(0xFF88AAFF);
		

//    	tick();
//    	translate(-150, -150, -150);
//		pointLight(255, 255, 255, p.xpos, p.ypos+10, p.zpos);
//		pointLight(255, 255, 255, p.xpos, p.ypos, p.zpos);
		worldView.camera(p.getXPos(), p.getZPos() - p.getCamheight(), p.getYPos(), (float) (p.getXPos() + Math.cos(x)),
				p.getZPos() + y - p.getCamheight(), (float) (p.getYPos() + Math.sin(x)), 0.0f, 1f, 0.0f);

		lvl.createlvl(worldView);
		selectBlock(worldView);
		worldView.endDraw();

		
	

		if (System.currentTimeMillis() - lastCleanTime > cleanTime)
		{
			lastCleanTime = System.currentTimeMillis();
			clean();
		}

//    		translate(p.getXPos()+cos(x)*2f, -p.getZPos()-y+p.camheight/100, p.getYPos()+sin(x)*2f);
//    		image(loadImage("res/hotbar.png","png"), 800 , 60);
//    		box(50);
//    		translate(-p.getXPos()+cos(x)*2f, +p.getZPos()+y*2f-p.camheight/100, -p.getYPos()+sin(x)*2f);

//        	translate( p.getXPos()+cos(x)*100, p.getZPos()+y*100-p.camheight, p.getYPos()+sin(x)*100);
//        	fill(0,0,0);
//        	box(50);
//        	translate(-(int) ((p.getXPos()+Math.cos(x))/100)*2, -(int) (-(p.getZPos()+y*12-p.camheight)/100), -(int) ((p.getYPos()+Math.sin(x))/100));

	}

	public void tick()
	{

		keyhandler();
		p.gravity();
		collision();
		p.calcmom();
//    	System.out.println("FPS :"+frameRate);
		x = x % (PI * 2);

	

	}

	public void clean()
	{
		ArrayList<Thing> tempActiveList = lvl.getWalls(); 	// save stuff locally to reduce calls
		ArrayList<Thing> tempDisabledList = lvl.getInactive();
		Thing tempThing;
		float wx;
		int thingXPos;
		int thingZPos;
		int pXPos = p.getXPos();
		int pYPos = p.getYPos();
		int pZPos = p.getZPos();
		float playerSin = sin(x);
		float playerCos = cos(x);
		float sscPosX = (pXPos - playerCos * 2048f);
		float sscPosY = (pYPos - playerSin * 2048f);

		for (int i = 0; i < tempActiveList.size(); i++)		// kick out active list
		{
			tempThing = tempActiveList.get(i);
			thingXPos = tempThing.getXPos();
			thingZPos = tempThing.getZPos();

			if ( sqrt(sq(thingXPos-pXPos)+sq(tempThing.getYPos()-pZPos)+sq(thingZPos-pYPos)) > renderDistance ||	// sqrt(a²+b²+c²)=d²
					(abs(sin(wx = atan2(thingZPos - sscPosY, tempThing.getXPos() - sscPosX)) - playerSin) > vboAngle
							&& abs(cos(wx) - playerCos) > vboAngle))
			{ 																										// get angle of object relative to viewing angle
				tempDisabledList.add(tempThing);
				tempActiveList.remove(i);
			}
		}

		for (int i = 0; i < tempDisabledList.size(); i++) 	// kick out disabled list
		{
			tempThing = tempDisabledList.get(i);
			thingXPos = tempThing.getXPos();
			thingZPos = tempThing.getZPos();

			if ( sqrt(sq(thingXPos-pXPos)+sq(tempThing.getYPos()-pZPos)+sq(thingZPos-pYPos)) < renderDistance &&	// sqrt(a²+b²+c²)=d²
					(abs(sin(wx = atan2(thingZPos - sscPosY, thingXPos - sscPosX)) - playerSin) < vboAngle
							&& abs(cos(wx) - playerCos) < vboAngle))
			{ 																										// get angle of object relative to viewing angle
				tempActiveList.add(tempThing);
				tempDisabledList.remove(i);
			}
		}
	}




	public void selectBlock(PGraphics worldView)
	{
		int vx = -1, vy = -1, vz = -1;
		p.setLookx(-1);
		p.setLooky(-1);
		p.setLookz(-1);
		for (float i = 0.45f; i < 5.5f; i += 0.15f)
		{
			if (	   
					lvl.getPane().length > 			(vx = round( p.getXPos() / Thing.defaultSize + cos(x) * i)) && vx > 0
					&& lvl.getPane()[0].length > 	(vy = round(-p.getZPos() / Thing.defaultSize - y * i + p.getCamheight() / Thing.defaultSize)) && vy > 0
					&& lvl.getPane()[0][0].length > (vz = round( p.getYPos() / Thing.defaultSize + sin(x) * i)) && vz > 0
					&& lvl.getPane()[vx][vy][vz] != 0
			)
			{
//				System.out.println("looking at : "+vx+" : "+vy+" : "+vz); 
				p.setLookx(vx);
				p.setLooky(vy);
				p.setLookz(vz);
				worldView.translate(	vx * Thing.defaultSize + Thing.defaultSize / 2, 
						   -vy * Thing.defaultSize - Thing.defaultSize / 2,
						   	vz * Thing.defaultSize + Thing.defaultSize / 2);
				worldView.fill(0x55000000);
				worldView.box(Thing.defaultSize + 128, Thing.defaultSize + 128, Thing.defaultSize + 128);
				armDistance = i - 0.25f;
				break;
			}

		}
	}

	public void setBlock(int blockID)
	{
		lvl.update(	round(p.getXPos() / Thing.defaultSize + cos(x) * armDistance),
					round(-p.getZPos() / Thing.defaultSize - y * armDistance + p.getCamheight() / Thing.defaultSize),
					round(p.getYPos() / Thing.defaultSize + sin(x) * armDistance), blockID);
	}

	public void keyhandler()
	{
		if (p.isSneaky() && (lvl.getPane()[0].length - 1 < -p.getZPos() / 100
				|| lvl.getPane()[p.getXPos() / 100][-p.getZPos() / 100 + 1][p.getYPos() / 100] == 0))
		{
			p.setYSize(1800);
			p.setCamheight(1650);
			p.setSneaky(false);
		}

		for (KeyEvent e : keycodes)
		{
			
//			System.out.println(e.getKeyCode());
			switch (e.getKeyCode())
			{

			case 87:
				p.setYmom(p.getYmom() + sin(x) * 16);
				p.setXmom(p.getXmom() + cos(x) * 16);
				break;
			case 83:
				p.setYmom(p.getYmom() - sin(x) * 16);
				p.setXmom(p.getXmom() - cos(x) * 16);
				break;
			case 65:
				p.setYmom(p.getYmom() - cos(x) * 16);
				p.setXmom(p.getXmom() + sin(x) * 16);
				break;
			case 68:
				p.setYmom(p.getYmom() + cos(x) * 16);
				p.setXmom(p.getXmom() - sin(x) * 16);
				break;
			case ' ':
				if (p.isOnGround())
				{
					p.setZacc(-28f);
					p.setIsonground(false);
				}
				break;
			case 16:
				if (!p.isSneaky())
				{
					p.setYSize(1500);
					p.setCamheight(1350);
					p.setSneaky(true);
				}
				break;
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:	currBBlock = e.getKeyCode()-49;
				break;
				
			case 99: debug = !debug;
				break;
			}
		}
	}
}