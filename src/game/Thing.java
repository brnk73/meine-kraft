package game;

public interface Thing extends Drawable, Cloneable {

	public static final int defaultSize = 1024;
	
	public int getXPos();
	public int getYPos();
	public int getZPos();
	
	public void setXPos(int xpos);
	public void setYPos(int ypos);
	public void setZPos(int zpos);
	
	public int getXSize();
	public int getYSize();
	public int getZSize();
	
	public void setXSize(int xsize);
	public void setYSize(int ysize);
	public void setZSize(int zsize);
	

	public void setImages(ImageSet set);

}
