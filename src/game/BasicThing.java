package game;
import processing.core.PGraphics;
import processing.core.PShape;
import processing.core.PConstants;

public class BasicThing implements Thing, Cloneable {

	private int xpos, ypos, zpos;
	private int xsize, ysize, zsize;

	private PShape objectShape;
	
	private float xPosHalfSize, yPosHalfSize, zPosHalfSize, halfPi, halfXSize, halfYSize, halfZSize, minusXSize, minusYSize, minusZSize;
	private ImageSet set;
	private int lastSides = -1;
	
	public BasicThing(int xpos, int ypos, int zpos) 
	{
		this(xpos, ypos, zpos, null);
	}
	public BasicThing(int xpos, int ypos, int zpos, ImageSet set) 
	{
		this(xpos, ypos, zpos, defaultSize, defaultSize, defaultSize, set);
	}
	public BasicThing(int xpos, int ypos, int zpos, int xsize, int ysize, int zsize) 
	{
		this(xpos, ypos, zpos, defaultSize, defaultSize, defaultSize, null);
	}
	
	public BasicThing(int xpos, int ypos, int zpos, int xsize, int ysize, int zsize, ImageSet set) 
	{
		this.xpos = xpos;
		this.ypos = ypos;
		this.zpos = zpos;
		this.xsize = xsize;
		this.ysize = ysize;
		this.zsize = zsize;
		this.set = set;
		
		halfXSize = xsize / 2;
		halfYSize = ysize / 2;	
		halfZSize = zsize / 2;
		minusXSize = -xsize;
		minusYSize = -ysize;
		minusZSize = -zsize;
		
		
		xPosHalfSize = xpos + halfXSize;
		yPosHalfSize = ypos - halfYSize;
		zPosHalfSize = zpos + halfZSize;
		
		halfPi = PGraphics.PI / 2;
	}

	@Override
	public int getXPos() {
		return xpos;
	}
	@Override
	public void setXPos(int xpos) {
		this.xpos = xpos;
	}
	@Override
	public int getYPos() {
		return ypos;
	}
	@Override
	public void setYPos(int ypos) {
		this.ypos = ypos;
	}
	@Override
	public int getZPos() {
		return zpos;
	}
	@Override
	public void setZPos(int zpos) {
		this.zpos = zpos;
	}
	@Override
	public int getXSize() {
		return xsize;
	}
	@Override
	public void setXSize(int xsize) {
		this.xsize = xsize;
	}
	@Override
	public int getYSize() {
		return ysize;
	}
	@Override
	public void setYSize(int ysize) {
		this.ysize = ysize;
	}
	@Override
	public int getZSize() {
		return zsize;
	}
	@Override
	public void setZSize(int zsize) {
		this.zsize = zsize;
	}


	@Override
	public void setImages(ImageSet set) {
		this.set = set;
		
	}

	@Override
	public void draw(PGraphics world, int sides) {
		
		if (lastSides != sides)
		{
			objectShape = world.createShape(0);

			PShape top = world.createShape();
			
			PShape bottom = world.createShape();
			
			PShape sideWalls = world.createShape();
			
			world.noStroke();
			world.textureMode(1);
			
			sideWalls.beginShape(17);
			sideWalls.texture(set.side);
			if ( (sides & 0b00_00_10) != 0 )
			{
				sideWalls.vertex(xpos, ypos, zpos, 0, 1);
				sideWalls.vertex(xpos + xsize, ypos, zpos, 1, 1);
				sideWalls.vertex(xpos + xsize, ypos - ysize, zpos, 1, 0);
				sideWalls.vertex(xpos, ypos - ysize, zpos, 0, 0);
			}
			if ( (sides & 0b00_10_00) != 0 )
			{	
				sideWalls.vertex(xpos, ypos, zpos + zsize, 0, 1);
				sideWalls.vertex(xpos, ypos, zpos,  1, 1);
				sideWalls.vertex(xpos, ypos - ysize, zpos, 1, 0);
				sideWalls.vertex(xpos, ypos - ysize, zpos + zsize, 0, 0);
			}
			if ( (sides & 0b00_00_01) != 0 )
			{
				sideWalls.vertex(xpos, ypos, zpos + zsize, 0, 1);
				sideWalls.vertex(xpos + xsize, ypos, zpos + zsize, 1, 1);
				sideWalls.vertex(xpos + xsize, ypos - ysize, zpos + zsize, 1, 0);
				sideWalls.vertex(xpos, ypos - ysize, zpos + zsize, 0, 0);
			}
			if ( (sides & 0b00_01_00) != 0 )
			{
				sideWalls.vertex(xpos+xsize, ypos, zpos + zsize, 0, 1);
				sideWalls.vertex(xpos+xsize, ypos, zpos,  1, 1);
				sideWalls.vertex(xpos+xsize, ypos - ysize, zpos, 1, 0);
				sideWalls.vertex(xpos+xsize, ypos - ysize, zpos + zsize, 0, 0);
			}
			sideWalls.endShape();
			
			if ( (sides & 0b01_00_00) != 0 )
			{
				top.beginShape();
				top.texture(set.top);
				top.vertex(xpos, ypos - ysize, zpos, 0, 1);
				top.vertex(xpos + xsize, ypos - ysize, zpos, 1, 1);
				top.vertex(xpos + xsize, ypos - ysize, zpos + zsize, 1, 0);
				top.vertex(xpos, ypos - ysize, zpos + zsize, 0, 0);
				top.endShape();
			}
			if ( (sides & 0b10_00_00) != 0 )
			{
				bottom.beginShape();
				bottom.texture(set.bottom);
				bottom.vertex(xpos, ypos, zpos, 0, 1);
				bottom.vertex(xpos + xsize, ypos, zpos, 1, 1);
				bottom.vertex(xpos + xsize, ypos, zpos + zsize, 1, 0);
				bottom.vertex(xpos, ypos, zpos + zsize, 0, 0);
				bottom.endShape();
			}
			
			objectShape.addChild(sideWalls);
			objectShape.addChild(top);
			objectShape.addChild(bottom);
			
			lastSides = sides;
		}
		
		if (set == null) return; 
		
		world.shape(objectShape);
	}


	
}
