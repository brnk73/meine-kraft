package game;

public class Movable extends BasicThing implements Thing {

	private float xacc, yacc, zacc;
	private float xmom, ymom, zmom;
	private float drag;
	
	public Movable(int xpos, int ypos, int zpos) {
		super(xpos, ypos, zpos);

		xacc = yacc = zacc = 0f;
		xmom = ymom = zmom = 0f;
		drag = 0.9f;
	}
	
	public Movable(int xpos, int ypos, int zpos,int xsize,int ysize, int zsize) {
		super(xpos, ypos, zpos, xsize, ysize, zsize);
		
		xacc = yacc = zacc = 0f;
		xmom = ymom = zmom = 0f;
		drag = 0.9f;
	}
	
	public void calcmom() {
		xmom += xacc;
		ymom += yacc;
		zmom += zacc;
		
		setXPos((int) (getXPos()+xmom));
		setYPos((int) (getYPos()+ymom));
		setZPos((int) (getZPos()+zmom));
		
		ymom *= drag; 
		xmom *= drag;
		zmom *= drag;
	}

	public float getXacc() {
		return xacc;
	}

	public void setXacc(float xacc) {
		this.xacc = xacc;
	}

	public float getYacc() {
		return yacc;
	}

	public void setYacc(float yacc) {
		this.yacc = yacc;
	}

	public float getZacc() {
		return zacc;
	}

	public void setZacc(float zacc) {
		this.zacc = zacc;
	}

	public float getXmom() {
		return xmom;
	}

	public void setXmom(float xmom) {
		this.xmom = xmom;
	}

	public float getYmom() {
		return ymom;
	}

	public void setYmom(float ymom) {
		this.ymom = ymom;
	}

	public float getZmom() {
		return zmom;
	}

	public void setZmom(float zmom) {
		this.zmom = zmom;
	}

	public float getDrag() {
		return drag;
	}

	public void setDrag(float drag) {
		this.drag = drag;
	}
	
	
	
}
