package game;
import processing.core.PGraphics;

public interface Drawable {

	public void draw(PGraphics world, int sides);
	
}
