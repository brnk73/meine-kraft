package game;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PConstants;

public class ItemThing implements Thing, Cloneable{

	private int xpos, ypos, zpos;
	private int xsize, ysize, zsize;
	private PImage image;
	private float xPosHalfSize, yPosHalfSize, zPosHalfSize, halfPi, quarterPi;
	
	public ItemThing(int xpos, int ypos, int zpos, PImage image) {
		this.xpos = xpos;
		this.ypos = ypos;
		this.zpos = zpos;
		xsize = defaultSize;
		zsize = defaultSize;
		ysize = defaultSize;
		this.image = image;
		
		xPosHalfSize = xpos + xsize / 2;
		yPosHalfSize = ypos - ysize / 2;
		zPosHalfSize = zpos + zsize / 2;
		halfPi = PGraphics.PI / 2;
		quarterPi = halfPi / 2;
	}
	
	@Override
	public void draw(PGraphics world, int sides) {	
		
//		fixes invisible objects behind ItemThings
		world.hint(PConstants.DISABLE_DEPTH_MASK);
//		world.pushMatrix();
//		world.translate(xPosHalfSize, yPosHalfSize, zPosHalfSize);
//		world.rotateY(quarterPi);
//		world.image(image, 0, 0, zsize, xsize);
//		world.rotateY(-halfPi);
//		world.image(image, 0, 0, zsize, xsize);
//		world.popMatrix();
		
		
		world.textureMode(1); 
		world.beginShape();
		world.texture(image);
		world.vertex(xpos, ypos, zpos, 0, 1);
		world.vertex(xpos + xsize, ypos, zpos + zsize, 1, 1);
		world.vertex(xpos + xsize, ypos - ysize, zpos + zsize, 1, 0);
		world.vertex(xpos, ypos - ysize, zpos, 0, 0);
		world.endShape();
		world.beginShape();
		world.texture(image);
		world.vertex(xpos, ypos, zpos + zsize, 0, 1);
		world.vertex(xpos + xsize, ypos, zpos,  1, 1);
		world.vertex(xpos + xsize, ypos - ysize, zpos, 1, 0);
		world.vertex(xpos, ypos - ysize, zpos + zsize, 0, 0);
		world.endShape();
		
		world.hint(PConstants.ENABLE_DEPTH_MASK);
	}

	@Override
	public int getXPos() {
		return xpos;
	}
	@Override
	public void setXPos(int xpos) {
		this.xpos = xpos;
	}
	@Override
	public int getYPos() {
		return ypos;
	}
	@Override
	public void setYPos(int ypos) {
		this.ypos = ypos;
	}
	@Override
	public int getZPos() {
		return zpos;
	}
	@Override
	public void setZPos(int zpos) {
		this.zpos = zpos;
	}
	@Override
	public int getXSize() {
		return xsize;
	}
	@Override
	public void setXSize(int xsize) {
		this.xsize = xsize;
	}
	@Override
	public int getYSize() {
		return ysize;
	}
	@Override
	public void setYSize(int ysize) {
		this.ysize = ysize;
	}
	@Override
	public int getZSize() {
		return zsize;
	}
	@Override
	public void setZSize(int zsize) {
		this.zsize = zsize;
	}


	@Override
	public void setImages(ImageSet set) {
		image = set.side;
		
	}
	
	
}
