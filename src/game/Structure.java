package game;

public interface Structure {

	public int getXpos();
	public void setXpos(int xpos);
	
	public int getYpos();
	public void setYpos(int ypos);
	
	public int getZpos();
	public void setZpos(int zpos);
	
	public int[][][] getData();
	public void setData(int[][][] data);
	
}
