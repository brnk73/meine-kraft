# Meine Kraft

![alt text](screenshots/2024-03-13.png)

Meine Kraft is a 3D java game with an interactive randomized level using the processing libary.

**Download Version 4**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[**MeineKraft v0.4**](https://git.thm.de/brnk73/meine-kraft/-/releases/v0.4)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
jre-17 min required


WASD to move, shift to duck, space to jump, left mouse button to destroy a block, right mouse button to set a block, scroll to select a different block, 1-7 to hotkey to a different block, esc to exit.
